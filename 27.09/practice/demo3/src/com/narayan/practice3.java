package com.narayan;

import java.util.ArrayList;

public class practice3 {

			public static void main(String[] args) {
				// Before Java 5.0
				ArrayList<String> list1 = new ArrayList();
				list1.add("Prashant");
				list1.add("Shubham");
				list1.add("Pratik");
				list1.add("Omkar");
				//list1.add(new Date()); // will cause ClassCastException on Line 21
				for(Object obj : list1) {
					String str = (String)obj;
					System.out.println(str);
				}
				System.out.println();
				
				// Since Java 5.0
				ArrayList<String> list2 = new ArrayList<String>();
				list2.add("Prashant");
				list2.add("Shubham");
				list2.add("Pratik");
				list2.add("Omkar");
				//list2.add(new Date()); // Compiler error on this line
				for(String obj : list2) {
					System.out.println(obj);
				}
				
				
			}

		}

