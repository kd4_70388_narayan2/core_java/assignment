package com.narayan;

class Arithmetic<T extends Number> {
	public double divide(T num1, T num2) {
		double result = num1.doubleValue() / num2.doubleValue();
		return result;
	}
}

public class practice4 {


			public static void main(String[] args) {
				Arithmetic<Integer> obj1 = new Arithmetic<>();
				double res1 = obj1.divide(22, 7);
				System.out.println("Result : " + res1);
				
				Arithmetic<Float> obj2;
				obj2 = new Arithmetic<>();
				double res2 = obj2.divide(22.0f, 7.0f);
				System.out.println("Result : " + res2);
				
				//Arithmetic<Character> obj3 = new Arithmetic<>(); // Compiler error
				//double res3 = obj1.divide('z', 'a');
				//System.out.println("Result : " + res1);
				
			}

		}


