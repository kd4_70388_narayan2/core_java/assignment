package com.narayan7;

import java.util.Arrays;
import java.util.Comparator;

// top-level class
class Employee implements Comparable<Employee> {
    private int empno;
    private String name;
    private int salary;
    public Employee() {
		// TODO Auto-generated constructor stub
	}
    
    public Employee(int empno, String name, int salary) {
		this.empno = empno;
		this.name = name;
		this.salary = salary;
	}
    
	public int getEmpno() {
		return empno;
	}

	public void setEmpno(int empno) {
		this.empno = empno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	// Natural Ordering of the class
	public int compareTo(Employee other) {
        int diff = this.empno - other.empno;
        //int diff = this.name.compareTo(other.name);
		return diff;
    }
	
	@Override
	public String toString() {
		return "Employee [empno=" + empno + ", name=" + name + ", salary=" + salary + "]";
	}
}

//top-level class
class EmployeeSalaryComparator implements Comparator<Employee> {
	@Override
	public int compare(Employee e1, Employee e2) {
		if(e1.getSalary() == e2.getSalary())
			return 0;
		if(e1.getSalary() > e2.getSalary())
			return +1;
		return -1;
	}
}

public class practice7 {

			public static void main(String[] args) {
				Employee[] arr = new Employee[] {
					new Employee(4, "Prashant", 400000),
					new Employee(3, "Nilesh", 700000),
					new Employee(1, "Nitin", 400000),
					new Employee(5, "Rahul", 600000),
					new Employee(2, "Rohan", 400000)
				};
				Arrays.sort(arr); // internally Arrays.sort(Object[]) -- arr[i].compareTo(arr[j]);
				for (Employee e : arr)
					System.out.println(e);
				System.out.println();
				
				EmployeeSalaryComparator salComp = new EmployeeSalaryComparator();
				Arrays.sort(arr, salComp); // internally Arrays.sort(T[], cmp) -- cmp.comapre(arr[i], arr[j]);
				for (Employee e : arr)
					System.out.println(e);
				System.out.println();
				
				class EmpSalNameComparator implements Comparator<Employee> {
					public int compare(Employee e1, Employee e2) {
						int diff = e1.getSalary() - e2.getSalary();
						if(diff == 0)
							diff = e1.getName().compareTo(e2.getName());
						return diff;
					}
				}
				Arrays.sort(arr, new EmpSalNameComparator());
				for (Employee e : arr)
					System.out.println(e);
				System.out.println();
				
				/*
				String[] sarr = { "Sameer", "Rahul", "Vishal", "Yogesh", "Shubham" };
				Arrays.sort(sarr);
				for (String s : sarr)
					System.out.println(s);
				System.out.println();
				
				
				
				// local class
				class StringDescComparator implements Comparator<String> {
					@Override
					public int compare(String s1, String s2) {
						int diff = s1.compareTo(s2);
						return -diff;
					}
				}
				
				StringDescComparator strDescComparator = new StringDescComparator();
				Arrays.sort(sarr, strDescComparator);
				for (String s : sarr)
					System.out.println(s);
				System.out.println();
				*/
			}

		}

