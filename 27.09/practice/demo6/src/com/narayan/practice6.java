package com.narayan;

public class practice6 {
	public static <T> void printArray(T[] arr) {
        for(T ele : arr)
            System.out.println(ele);
        System.out.println("Number of elements printed: " + arr.length);
    }
    
    public static <T extends Number> T getMax(T[] arr) {
    	T max = arr[0];
    	for(T num:arr) {
    		if(num.doubleValue() > max.doubleValue())
    			max = num;
    	}
    	return max;
    }


	public static void main(String[] args) {
		    
			    String[] arr1 = { "John", "Dagny", "Alex" };
			    printArray(arr1); // printArray<String> -- String type is inferred

			    Integer[] arr2 = { 10, 20, 30 };
			    printArray(arr2); // printArray<Integer> -- Integer type is inferred		
			    int maxInt = getMax(arr2);
			    System.out.println("Max : " + maxInt);
			}
		}

