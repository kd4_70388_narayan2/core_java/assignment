package com.narayan5;

import java.util.Date;

class Box<T> {
    private T obj;
    public Box(T obj) {
        this.obj = obj;
    }
    public T get() {
        return this.obj;
    }
    public void set(T obj) {
        this.obj = obj;
    }
}

public class practice5 {
	public static void printBox(Box<?> b) {
        Object obj = b.get();
        System.out.println("Box contains: " + obj);
    }
    
    // ? -- Any class that is Number or Inherited from Number
    public static void printBox1(Box<? extends Number> b) {
        Number obj = b.get();
        System.out.println("Box1 contains: " + obj);
    }

    // ? -- Any class that is Number or Super class of Number
    public static void printBox2(Box<? super Number> b) {
        Object obj = b.get();
        System.out.println("Box2 contains: " + obj);
    }
		  
			public static void main(String[] args) {	
				Box<String> b1 = new Box<>("Nilesh");
				printBox(b1);
				//printBox1(b1);	// compiler error
				//printBox2(b1);	// compiler error
				Box<Date> b2 = new Box<>(new Date());
				printBox(b2);
				//printBox1(b2);	// compiler error
				//printBox2(b2);	// compiler error
				Box<Integer> b3 = new Box<>(123);
				printBox(b3);
				printBox1(b3);
				//printBox2(b3);	// compiler error
				Box<?> b4 = new Box<Double>(123.4);
				printBox(b4);
				//printBox1(b4);	// compiler error -- any type (?) is not necessarily inherited from Number
				//printBox2(b4);	// compiler error -- any type (?) is not necessarily super class of Number	
				Box<Float> b5 = new Box<Float>(123.456f);
				printBox(b5);
				printBox1(b5);
				//printBox2(b5);	// compiler error 
				
				Box<Object> b6 = new Box<Object>(121);
				printBox(b6);
				//printBox1(b6);	// compiler error : Object is not exteneded from Number
				printBox2(b6);	// okay: Object is super class of Number
				
			}

		}
