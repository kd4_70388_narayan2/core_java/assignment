package com.narayan;
import java.util.Date;

class Box {
    private Object obj;
    public void set(Object obj) {
        this.obj = obj;
    }
    public Object get() {
        return this.obj;
    }
}

public class Demo1 {

	public static void main(String[] args) {
	    Box b1 = new Box();
	    b1.set("Nilesh");
	    String obj1 = (String)b1.get();
	    System.out.println("obj1 : " + obj1);

	    Box b2 = new Box();
	    b2.set(new Date());
	    Date obj2 = (Date)b2.get();
	    System.out.println("obj2 : " + obj2);

	    Box b3 = new Box();
        b3.set(new Integer(11));
	    String obj3 = (String)b3.get();  // ClassCastException
	    System.out.println("obj3 : " + obj3);
	}

}
